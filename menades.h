#pragma once

#include <gtkmm/window.h>
#include <gtkmm/overlay.h>
#include <gtkmm/grid.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/comboboxtext.h>

#include "matriz.h"

class Menades : public Gtk::Window
{
public:
  Menades();
  virtual ~Menades();

private:
  // Signal handlers:

  // Child widgets:
  Gtk::Grid inexterna;
  Gtk::Overlay overlay;
  Matriz m;

  Gtk::Label pixels1;
  Gtk::Label pixels2;
  Gtk::Label x1;
  Gtk::Label x2;

  Gtk::Label semillas_label;
  Gtk::ComboBoxText semillas_drop;
  Gtk::Grid rsemillas_drop;

//  resolucion semilla |____| x |____| px
  Gtk::Label res_semilla_label;
  Gtk::Grid rsemilla_input;
  Gtk::Entry res_semillax;
  Gtk::Entry res_semillay;
  
//  resolucion dibujo |____| x |____| px
  Gtk::Label res_dibujo_label;
  Gtk::Grid rdibujo_input; 
  Gtk::Entry res_dibujox;
  Gtk::Entry res_dibujoy;

  
  //parametro :o) 
  std::string semilla;
  std::tuple<int,int> resolucion_sem, resolucion_dib;
};

