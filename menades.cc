#include <iostream>
#include <set>

#include <gtkmm/comboboxtext.h>
#include <dirent.h>

#include "menades.h"
#include "matriz.h"

void GetFilesInDir(Gtk::ComboBoxText *semillas_drop)
{
    DIR *dir;
    struct dirent *ent;

    dir = opendir("/home/sacul/Code/Haskell/lengdib/img");
    if (dir == NULL) {
        return;
    }

    std::set<std::string> my_semillas;

    while ((ent = readdir(dir)) != NULL) {
        const std::string file_name = ent->d_name;

        if (file_name[0] == '.')
            continue;
        
        if (file_name.substr(file_name.find_last_of(".") + 1) == "bmp") {
            my_semillas.insert(file_name);
        }
    }

    std::for_each(my_semillas.begin(),
                  my_semillas.end(),
                  [semillas_drop] (std::string name) { semillas_drop->append(name); } );

    closedir(dir);
}

Menades::Menades()
 : pixels1("px"),
   pixels2("px"),
   x1(" x "),
   x2(" x "),
  semillas_label("selección de semilla"),
  res_semilla_label("resolución semilla"),
  res_dibujo_label("resolución dibujo")
{
  set_title("Menades");
  set_border_width(12);

  add(overlay);

  overlay.add(m);
  overlay.add_overlay(inexterna);
  
  inexterna.set_halign(Gtk::ALIGN_CENTER);
  inexterna.set_valign(Gtk::ALIGN_CENTER);
  inexterna.set_orientation(Gtk::ORIENTATION_VERTICAL);

  inexterna.add(rsemillas_drop);
  inexterna.add(rsemilla_input);
  inexterna.add(rdibujo_input);

  rsemillas_drop.add(semillas_label);
  rsemillas_drop.add(semillas_drop);
  GetFilesInDir(&semillas_drop);
  semillas_drop.set_active(1);

  rsemilla_input.add(res_semilla_label); 
  rsemilla_input.add(res_semillax); 
  rsemilla_input.add(x1);
  rsemilla_input.add(res_semillay);
  rsemilla_input.add(pixels1);

  rdibujo_input.add(res_dibujo_label); 
  rdibujo_input.add(res_dibujox); 
  rdibujo_input.add(x2);
  rdibujo_input.add(res_dibujoy); 
  rdibujo_input.add(pixels2); 

  show_all_children();
}

Menades::~Menades()
{
}
