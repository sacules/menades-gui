# Ménades

## Requisitos
Ménades utiliza **Meson** para compilar el proyecto y **GTK+3** como librería
gráfica, los cuales se pueden instalar (en Debian y Ubuntu) de la siguiente
forma:

```console
sudo apt update
sudo apt install meson ninja libgtkmm-3.0-dev
```

## Compilar
En la raíz del proyecto, escribir en la terminal:

```console
mkdir build
meson build
```

Ésto va a leer la configuración de `meson.build`, y generar el directorio
`build`, que es donde se guardarán los binarios generados. Sólo hace falta
usar éste comando una sola vez al principio. Para compilar:

```console
ninja -C build
```

Ésto es lo único necesario cada vez que se modifica un archivo y se quiere 
recompilar.

## Ejecutar
```console
./build/menades
```

## Etc.
El proceso anterior es igualmente aplicable a los proyectos de ejemplo.
