#include <gtkmm/application.h>
#include <gtkmm/overlay.h>
#include <gtkmm/window.h>

#include "menades.h"

int main(int argc, char *argv[]) {
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.matriz.menades");

    Menades m;

    return app->run(m);
}
