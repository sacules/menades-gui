#include <cairomm/context.h>
#include <glibmm/main.h>
#include <cstdlib>
#include <ctime>

#include "matriz.h"

Matriz::Matriz() {
    this->radius = 0.5;
    this->line_width = 3;
    this->line_gap = 6;
}

Matriz::~Matriz() {}

bool Matriz::on_draw(const Cairo::RefPtr<Cairo::Context> &cr) {
    Gtk::Allocation allocation = get_allocation();
    const int screen_width = allocation.get_width();
    const int screen_height = allocation.get_height();

    cr->set_line_width(line_width);
    cr->set_source_rgb(0, 0, 0);  // white
    cr->paint();

    cr->arc(screen_width / 2, screen_height / 2, screen_width / 3, 0, 2*M_PI);
    cr->move_to(0, 0);
    cr->line_to(0, screen_height);
    cr->line_to(screen_width, screen_height);
    cr->line_to(screen_width, 0);
    cr->line_to(0, 0);
    cr->clip();
    cr->begin_new_path();
    cr->set_source_rgb(1, 1, 1);

    // use current time as seed for random generator
    std::srand(std::time(nullptr));

    // Horizontal lines
    for (int y = line_width - 0.5; y < screen_height; y += line_width + line_gap) {
        // Use this to get a small random number
        int len = std::rand() % 10 + 1;

        std::vector<double> dash_array(len);
        for (int i = 0; i < len; i++) {
            dash_array[i] = (double)(std::rand() % 50 + 1);
        }

        cr->set_dash(dash_array, 0);
        cr->move_to(0, y);
        cr->line_to(screen_width, y);
        cr->stroke();
    }

    // Vertical lines
    for (int x = line_width - 0.5; x < screen_width; x += line_width + line_gap) {
        // Use this to get a small random number
        int len = std::rand() % 10 + 1;

        std::vector<double> dash_array(len);
        for (int i = 0; i < len; i++) {
            dash_array[i] = (double)(std::rand() % 50 + 1);
        }

        cr->set_dash(dash_array, 0);
        cr->move_to(x, 0);
        cr->line_to(x, screen_height);
        cr->stroke();
    }

    Pango::FontDescription font;

    font.set_family("Monospace");
    font.set_weight(Pango::WEIGHT_HEAVY);

    auto layout = create_pango_layout("0 0 0\n 0 0\n  0");

    layout->set_font_description(font);
    
    int text_width;
    int text_height;

    layout->get_pixel_size(text_width, text_height);

    cr->move_to((screen_width-text_width)/2, 0);

    layout->show_in_cairo_context(cr);

    return true;
}
