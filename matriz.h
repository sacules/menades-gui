#pragma once

#include <gtkmm/drawingarea.h>


class Matriz : public Gtk::DrawingArea {
   public:
    Matriz();
    virtual ~Matriz();

   protected:
    // Override default signal handler:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

    bool on_timeout();

    double radius;
    int line_width;
    int line_gap;
};
