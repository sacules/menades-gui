#include <gtkmm/application.h>

#include "helloworld.h"

int main(int argc, char *argv[]) {
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.caca.karen");

    HelloWorld hello;
    hello.set_default_size(690, 420);

    return app->run(hello);
}
