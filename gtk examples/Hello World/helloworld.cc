#include <iostream>

#include "helloworld.h"

HelloWorld::HelloWorld() {
    this->m_button = Gtk::Button("holis");

    this->cacas = {"😲", "🙄", "😠", "😢", "🤔", "🤗"};

    this->click_count = 0;
    // Sets the border width of the window.
    set_border_width(10);

    // When the button receives the "clicked" signal, it will call the
    // on_button_clicked() method defined below.
    m_button.signal_clicked().connect(
        sigc::mem_fun(*this, &HelloWorld::on_button_clicked));

    // This packs the button into the Window (a container).
    this->add(m_button);

    // The final step is to display this newly created widget...
    m_button.show();
}

HelloWorld::~HelloWorld() {}

void HelloWorld::on_button_clicked() {
    int position = click_count % cacas.size();
    m_button.set_label(cacas[position]);
    m_button.override_color(Gdk::RGBA("red"));
    click_count++;
}
