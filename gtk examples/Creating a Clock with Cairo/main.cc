#include <gtkmm/application.h>
#include <gtkmm/button.h>
#include <gtkmm/overlay.h>
#include <gtkmm/window.h>

#include "clock.h"

/* {"","🙄","","😢","🤔","🤗"}; */

int main(int argc, char** argv) {
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    Gtk::Window win;
    win.set_title("Cairomm Clock");

    Gtk::Overlay overlay;

    Clock c;
    overlay.add(c);

    Gtk::Button b1 = Gtk::Button("🤗");
    b1.set_halign(Gtk::Align::ALIGN_CENTER);
    b1.set_valign(Gtk::Align::ALIGN_CENTER);

    Gtk::Button b2 = Gtk::Button("😲");
    b2.set_halign(Gtk::Align::ALIGN_START);
    b2.set_valign(Gtk::Align::ALIGN_CENTER);

    Gtk::Button b3 = Gtk::Button("😠");
    b3.set_halign(Gtk::Align::ALIGN_END);
    b3.set_valign(Gtk::Align::ALIGN_CENTER);

    overlay.add_overlay(b1);
    overlay.add_overlay(b2);
    overlay.add_overlay(b3);

    win.add(overlay);
    win.show_all_children();

    return app->run(win);
}
